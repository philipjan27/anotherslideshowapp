package slideshow.app.edgar.anotherslideshowapp;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.bumptech.glide.Glide;


/**
 * Created by Philipp on 10/05/2017.
 */

public class CustomPagerAdapter extends PagerAdapter {

    onViewClicked callback;
    View view;
    LayoutInflater inflater;
    private Context context;
    private int [] drawables = {R.drawable.image_01,R.drawable.image_02,R.drawable.image_03,R.drawable.image_04,R.drawable.image_05,R.drawable.image_06,R.drawable.image_07};

    public CustomPagerAdapter(Context ctx) {
        this.context=ctx;
    }

    @Override
    public int getCount() {
        return drawables.length;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return (view == object);
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((LinearLayout)object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        inflater= (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        view= inflater.inflate(R.layout.image_resourceview,container,false);
        ImageView imageView= (ImageView)view.findViewById(R.id.imageview_im);
        Glide.with(view.getContext()).load(drawables[position]).into(imageView);
        container.addView(view);

        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                callback.clicked();
            }
        });

        view.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                callback.touched();
                return false;
            }
        });

        view.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                callback.longPresses();
                return true;
            }
        });

//        callback.getCurrentPosition(position);
        return view;
    }

    interface onViewClicked{
        void clicked();
        void touched();
        void longPresses();
    }

    public void setViewOnClickListener(onViewClicked callback) {
        this.callback= callback;
    }

}
