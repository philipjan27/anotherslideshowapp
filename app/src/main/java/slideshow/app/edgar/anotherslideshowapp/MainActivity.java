package slideshow.app.edgar.anotherslideshowapp;

import android.app.Activity;
import android.os.Handler;
import android.support.v4.graphics.drawable.TintAwareDrawable;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.LinearLayout;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

public class MainActivity extends Activity {
    ViewPager viewPager;
    CustomPagerAdapter adapter;
    LinearLayout parentLayout;
    int currentPosition;
    int count;

    Handler h;
    Runnable r;

    Timer timer;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_main);

        parentLayout = (LinearLayout) findViewById(R.id.parentlayout);
        viewPager = (ViewPager) findViewById(R.id.viewpager_image);
        adapter = new CustomPagerAdapter(this);
        viewPager.setAdapter(adapter);
        viewPager.setCurrentItem(0);
        viewPager.setOffscreenPageLimit(6);
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                Log.d(MainActivity.class.getSimpleName(), "POSITION: onPageSelected: " + position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        adapter.setViewOnClickListener(new CustomPagerAdapter.onViewClicked() {
            @Override
            public void clicked() {
                Log.d(MainActivity.class.getSimpleName(), "Mainactivity callback from adapter");
//                hideNavBar();
            }

            @Override
            public void touched() {
//                hideNavBar();
            }

            @Override
            public void longPresses() {

            }

        });

    }

    @Override
    protected void onResume() {
//        hideNavBar();
//        initializeTimer();
        initUsingHandler();
        super.onResume();
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        View decorView = getWindow().getDecorView();
        if (hasFocus) {
            decorView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                    | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
                    | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                    | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                    | View.SYSTEM_UI_FLAG_FULLSCREEN
                    | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION);
        }
    }

    private void initializeTimer() {
        Timer timer = new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                /*        currentPosition = viewPager.getCurrentItem();
                        Log.d(MainActivity.class.getSimpleName(), "POSITION: BEFORE" + currentPosition);
                        currentPosition++;
                        Log.d(MainActivity.class.getSimpleName(), "POSITION: AFTER" + currentPosition);
                        if (currentPosition >= 7) {
                            viewPager.setCurrentItem(0);
                            currentPosition = 0;
                        }
                        viewPager.setCurrentItem(currentPosition);*/

                        currentPosition = viewPager.getCurrentItem();
                        Log.d(MainActivity.class.getSimpleName(), "POSITION: BEFORE" + currentPosition);
                        currentPosition++;
                        Log.d(MainActivity.class.getSimpleName(), "POSITION: AFTER" + currentPosition);
                        if (currentPosition >= 7) {
                            viewPager.setCurrentItem(0);
                            currentPosition = 0;
                        }
                        viewPager.setCurrentItem(currentPosition - 1);

                    }
                });
            }
        }, 0, 15000);
    }

    private void initUsingHandler() {
        h = new Handler();
        r = new Runnable() {
            @Override
            public void run() {

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        if (currentPosition > 6) {
                            viewPager.setCurrentItem(0);
                            currentPosition = viewPager.getCurrentItem();
                        } else {
                            Log.d(MainActivity.class.getSimpleName(), "POSITION: BEFORE" + currentPosition);
                            viewPager.setCurrentItem(currentPosition);
                            currentPosition = viewPager.getCurrentItem()+1;
                            Log.d(MainActivity.class.getSimpleName(), "POSITION: AFTER" + currentPosition);

                        }


                    }
                });

                h.postDelayed(r, 15000);
            }
        };

        h.post(r);
    }


}
